#!/bin/bash

keytool -genkey -v -keystore manaplus-release-key.keystore -alias manaplus -keyalg RSA -keysize 2048 -validity 10000
