#!/bin/bash

jarsigner -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore tools/manaplus-release-key.keystore \
project/bin/MainActivity-release-unsigned.apk manaplus

zipalign -v 4 project/bin/MainActivity-release-unsigned.apk project/bin/ManaPlus.apk
