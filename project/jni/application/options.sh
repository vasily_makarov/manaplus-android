#!/bin/bash

#-Wcast-align -Wundef

#-Wself-assign -Wself-assign-non-pod -Wripa-opt-mismatch

export CXXFLAGS="-std=c++11 -Wc++11-compat -Wmaybe-uninitialized \
-O3 -pipe -ffast-math -funswitch-loops -Wvla \
-Wpacked-bitfield-compat -Wtrampolines \
-Wsuggest-attribute=noreturn -Wunused -Wstrict-aliasing=2 \
-fstrict-aliasing -Wunreachable-code -Wabi -Wdisabled-optimization \
-Wvolatile-register-var -Winvalid-pch \
-Wnormalized=nfkc -Wmissing-format-attribute -Wmissing-noreturn \
-Wswitch-default -Wsign-promo -Waddress -Wmissing-declarations \
-Wctor-dtor-privacy -Wstrict-null-sentinel -Wlogical-op \
-Wpointer-arith -Wmissing-include-dirs -Winit-self -pedantic -Wall \
-Wpacked -Wextra -fstrict-overflow -Wstrict-overflow=1 -Wunknown-pragmas \
-Wwrite-strings -Wstack-protector -Wunused-macros -Wsynth \
-Wbuiltin-macro-redefined -Wctor-dtor-privacy -Wdeprecated -Wextra \
-Wendif-labels -Wformat=1 -Wimport -Wnon-virtual-dtor -Wpsabi \
-Wsign-promo -Wwrite-strings -D_FORTIFY_SOURCE=2 -Wc++11-compat \
-Wmaybe-uninitialized -Wnarrowing \
-Wno-long-long -Wno-variadic-macros"

