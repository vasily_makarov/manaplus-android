#!/bin/sh

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

cd "$LOCAL_PATH"

mkdir -p AndroidData
rm -f AndroidData/manaplus-data.zip

cd manaplus
zip "$LOCAL_PATH/AndroidData/manaplus-data.zip" -r data -x "*Makefile*" -x "*CMake*"

cd ../temp/share
zip "$LOCAL_PATH/AndroidData/manaplus-data.zip" -r locale
