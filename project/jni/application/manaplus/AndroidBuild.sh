#!/bin/sh

LOCAL_PATH=`dirname $0`
LOCAL_PATH=`cd $LOCAL_PATH && pwd`

rm "$LOCAL_PATH/libapplication.so"

ln -sf libsdl-1.2.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL.so
ln -sf libsdl_gfx.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_gfx.so
ln -sf libsdl_image.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_image.so
ln -sf libsdl_mixer.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_mixer.so
ln -sf libsdl_net.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_net.so
ln -sf libsdl_sound.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_sound.so
ln -sf libsdl_ttf.so $LOCAL_PATH/../../../obj/local/armeabi/libSDL_ttf.so

if [ \! -f manaplus/configure ] ; then
	sh -c "cd manaplus && autoreconf -i"
fi

if [ \! -f manaplus/configure ] ; then
	exit 1
fi

PREFIX=`pwd`/temp
rm -rf "$PREFIX"
mkdir "$PREFIX"

if [ \! -f manaplus/Makefile ] ; then
	../setEnvironment-r5b.sh sh -c "cd manaplus && ./configure --prefix=$PREFIX --enable-androidbuild --build=x86_64-unknown-linux-gnu --host=arm-eabi"
fi

if [ \! -f manaplus/Makefile ] ; then
	exit 1
fi

NCPU=`cat /proc/cpuinfo | grep -c -i processor`
../setEnvironment-r5b.sh sh -c "cd manaplus && make -j$NCPU" && cp -f manaplus/src/manaplus libapplication.so
../setEnvironment-r5b.sh sh -c "cd manaplus/po && make update-gmo && make install"

rm -rf "$LOCAL_PATH/AndroidData"
./AndroidData.sh

if [ \! -f libapplication.so ] ; then
	exit 1
fi

exit 0
