APP_PROJECT_PATH := $(call my-dir)/..

APP_STL := gnustl_shared
APP_GNUSTL_CPP_FEATURES := exceptions rtti
APP_CFLAGS := -O3 -DNDEBUG -g # arm-linux-androideabi-4.4.3 crashes in -O0 mode on SDL sources
APP_PLATFORM := android-9
NDK_TOOLCHAIN_VERSION=4.8

include jni/Settings.mk

