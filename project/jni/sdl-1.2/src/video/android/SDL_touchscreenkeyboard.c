/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/
#include <jni.h>
#include <android/log.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>
#include <math.h>
#include <string.h> // for memset()
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <netinet/in.h>

#include "SDL_config.h"

#include "SDL_version.h"

//#include "SDL_opengles.h"
#include "SDL_screenkeyboard.h"
#include "../SDL_sysvideo.h"
#include "SDL_androidvideo.h"
#include "SDL_androidinput.h"
#include "jniwrapperstuff.h"

// #include "touchscreentheme.h" // Not used yet

// TODO: this code is a HUGE MESS

static int
power_of_2(int input)
{
    int value = 1;

    while (value < input) {
        value <<= 1;
    }
    return value;
}

int SDL_ANDROID_ToggleScreenKeyboardTextInput(const char * previousText)
{
//+
	static char textIn[255];
	if( previousText == NULL )
		previousText = "";
	strncpy(textIn, previousText, sizeof(textIn));
	textIn[sizeof(textIn)-1] = 0;
	SDL_ANDROID_CallJavaShowScreenKeyboard(textIn, NULL, 0);
	return 1;
};

// That's probably not the right file to put this func
JNIEXPORT jint JNICALL
JAVA_EXPORT_NAME(Settings_nativeChmod) ( JNIEnv*  env, jobject thiz, jstring j_name, jint mode )
{
    jboolean iscopy;
    const char *name = (*env)->GetStringUTFChars(env, j_name, &iscopy);
    int ret = chmod(name, mode);
    (*env)->ReleaseStringUTFChars(env, j_name, name);
    return (ret == 0);
}

JNIEXPORT void JNICALL
JAVA_EXPORT_NAME(Settings_nativeSetEnv) ( JNIEnv*  env, jobject thiz, jstring j_name, jstring j_value )
{
    jboolean iscopy;
    const char *name = (*env)->GetStringUTFChars(env, j_name, &iscopy);
    const char *value = (*env)->GetStringUTFChars(env, j_value, &iscopy);
    setenv(name, value, 1);
    (*env)->ReleaseStringUTFChars(env, j_name, name);
    (*env)->ReleaseStringUTFChars(env, j_value, value);
}

int SDLCALL SDL_HasScreenKeyboardSupport(void *unused)
{
	return 1;
}

// SDL2 compatibility
int SDLCALL SDL_ShowScreenKeyboard(void *unused)
{
	return SDL_ANDROID_ToggleScreenKeyboardTextInput(NULL);
}

int SDLCALL SDL_HideScreenKeyboard(void *unused)
{
	SDL_ANDROID_CallJavaHideScreenKeyboard();
	return 1;
}

int SDLCALL SDL_IsScreenKeyboardShown(void *unused)
{
	return SDL_ANDROID_IsScreenKeyboardShown();
}

int SDLCALL SDL_GetScreenKeyboardHeight(void *unused)
{
	return SDL_ANDROID_CallJavaGetScreenKeyboardHeight();
}

int SDLCALL SDL_ToggleScreenKeyboard(void *unused)
{
	if( SDL_IsScreenKeyboardShown(NULL) )
		return SDL_HideScreenKeyboard(NULL);
	else
		return SDL_ShowScreenKeyboard(NULL);
}
