#include "SDL_config.h"

#include "SDL_version.h"
#include "SDL_androidinput.h"
#include "SDL_screenkeyboard.h"

void SDL_android_init_keymap_shift(Uint16 *SDL_android_keymap_shift) {

	Uint16 *keymap = SDL_android_keymap_shift;

	int i;
	for (i = SDLK_FIRST; i <= SDLK_LAST; i++) {
		keymap[i] = i;
	}

	keymap[SDL_KEY(A)] = 'A';
	keymap[SDL_KEY(B)] = 'B';
	keymap[SDL_KEY(C)] = 'C';
	keymap[SDL_KEY(D)] = 'D';
	keymap[SDL_KEY(E)] = 'E';
	keymap[SDL_KEY(F)] = 'F';
	keymap[SDL_KEY(G)] = 'G';
	keymap[SDL_KEY(H)] = 'H';
	keymap[SDL_KEY(I)] = 'I';
	keymap[SDL_KEY(J)] = 'J';
	keymap[SDL_KEY(K)] = 'K';
	keymap[SDL_KEY(L)] = 'L';
	keymap[SDL_KEY(M)] = 'M';
	keymap[SDL_KEY(N)] = 'N';
	keymap[SDL_KEY(O)] = 'O';
	keymap[SDL_KEY(P)] = 'P';
	keymap[SDL_KEY(Q)] = 'Q';
	keymap[SDL_KEY(R)] = 'R';
	keymap[SDL_KEY(S)] = 'S';
	keymap[SDL_KEY(T)] = 'T';
	keymap[SDL_KEY(U)] = 'U';
	keymap[SDL_KEY(V)] = 'V';
	keymap[SDL_KEY(W)] = 'W';
	keymap[SDL_KEY(X)] = 'X';
	keymap[SDL_KEY(Y)] = 'Y';
	keymap[SDL_KEY(Z)] = 'Z';

	keymap[SDL_KEY(1)] = '!';
	keymap[SDL_KEY(2)] = '@';
	keymap[SDL_KEY(3)] = '#';
	keymap[SDL_KEY(4)] = '$';
	keymap[SDL_KEY(5)] = '%';
	keymap[SDL_KEY(6)] = '^';
	keymap[SDL_KEY(7)] = '&';
	keymap[SDL_KEY(8)] = '*';
	keymap[SDL_KEY(9)] = '(';
	keymap[SDL_KEY(0)] = ')';
	keymap['`'] = '~';

	keymap[SDL_KEY(QUOTE)] 		= '"';
	keymap[SDL_KEY(SLASH)] 		= '?';
	keymap[SDL_KEY(MINUS)] 		= '_';
	keymap[SDL_KEY(SEMICOLON)] 	= ':';
	keymap[SDL_KEY(COMMA)] 		= '<';
	keymap[SDL_KEY(PERIOD)] 	= ':';
	keymap[SDL_KEY(EQUALS)] 	= '+';
	keymap[SDL_KEY(GRAVE)] 		= '~';
	keymap[SDL_KEY(BACKSLASH)] 	= '|';
	keymap[SDL_KEY(LEFTBRACKET)] 	= '{';
	keymap[SDL_KEY(RIGHTBRACKET)] 	= '}';

}
