/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2009 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#ifndef _SDL_screenkeyboard_h
#define _SDL_screenkeyboard_h

#include "SDL_stdinc.h"
#include "SDL_video.h"
#include "SDL_version.h"
#if SDL_VERSION_ATLEAST(1,3,0)
#include "SDL_keyboard.h"
#include "SDL_keycode.h"
#include "SDL_scancode.h"
#else
#include "SDL_keysym.h"
#endif

/* On-screen keyboard exposed to the application, it's available on Android platform only */

#include "begin_code.h"
/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C" {
#endif

/* All functions return 0 on failure and 1 on success, contrary to other SDL API.
   On the other hand, those functions actually never fail, so you may skip error checking. */

/* Buttons 0 and 1 may have auto-fire state */

extern DECLSPEC int SDLCALL SDL_ANDROID_SetScreenKeyboardShown(int shown);
extern DECLSPEC int SDLCALL SDL_ANDROID_GetScreenKeyboardShown(void);

extern DECLSPEC int SDLCALL SDL_ANDROID_GetScreenKeyboardSize(void);

/* Show Android on-screen keyboard, and pass entered text back to application as SDL keypress events,
previousText is UTF-8 encoded, it may be NULL, only 256 first bytes will be used, and this call will not block */
extern DECLSPEC int SDLCALL SDL_ANDROID_ToggleScreenKeyboardTextInput(const char * previousText);

/* Show only the bare Android on-screen keyboard without any text input field, so it won't cover the screen */
extern DECLSPEC int SDLCALL SDL_ANDROID_ToggleScreenKeyboardWithoutTextInput(void);

/* Show Android on-screen keyboard, and pass entered text back to application in a buffer,
using buffer contents as previous text (UTF-8 encoded), the buffer may be of any size -
this call will block until user typed all text. */

/* Whether user redefined on-screen keyboard layout via SDL menu, app should not enforce it's own layout in that case */
extern DECLSPEC int SDLCALL SDL_ANDROID_GetScreenKeyboardRedefinedByUser(void);

/* API compatible to SDL2, it's a wrapper to the SDL_ANDROID_ToggleScreenKeyboardWithoutTextInput(), it does not block */

extern DECLSPEC int SDLCALL SDL_HasScreenKeyboardSupport(void *unused);

extern DECLSPEC int SDLCALL SDL_ShowScreenKeyboard(void *unused);

extern DECLSPEC int SDLCALL SDL_HideScreenKeyboard(void *unused);

extern DECLSPEC int SDLCALL SDL_ToggleScreenKeyboard(void *unused);

extern DECLSPEC int SDLCALL SDL_IsScreenKeyboardShown(void *unused);

extern DECLSPEC int SDLCALL SDL_ANDROID_ToggleScreenKeyboardTextInput(const char * previousText);

extern DECLSPEC int SDLCALL SDL_GetScreenKeyboardHeight(void *unused);

extern DECLSPEC int SDLCALL SDL_ANDROID_OpenBrowser(const char *url);

#ifdef __cplusplus
}
#endif
#include "close_code.h"

#endif
