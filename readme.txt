This is ManaPlus Android port building environment.
To build ManaPlus for Android just follow these steps:
1) Download and install android-ndk (r5b or higher)
2) Make a symbolic link to manaplus source folder in /project/jni/application/manaplus/ directory:
   $ ln -s /path/to/manaplus/source project/jni/application/manaplus/manaplus
3) Launch build.sh script from repo root folder:
   $ cd /path/to/repo/root
   $ ./build.sh
   Use -i flag to install application through adb or -r to run it
